package com.teaminternational.jaguar;

import org.testng.Assert;
import org.testng.annotations.Test;


public class TestCalculator {

    @Test
    public void testAdd() {
        Calculator calculator = new Calculator();
        Assert.assertEquals(calculator.add(2.0,2.0),4.0);
    }

    @Test
    public void testSubstruct() {
        Calculator calculator = new Calculator();
        Assert.assertEquals(calculator.substruct(4.0,2.0),2.0);
    }

    @Test
    public void testDivide() {
        Calculator calculator = new Calculator();
        Assert.assertEquals(calculator.divide(6.0,2.0),3.0);
    }

    @Test
    public void testMultiply() {
        Calculator calculator = new Calculator();
        Assert.assertEquals(calculator.multiply(3.0,2.0),6.0);
    }
    @Test
    public void testAddFail() {
        Calculator calculator = new Calculator();
        Assert.assertEquals(calculator.add(2.0,2.0),7.0);
    }

    @Test
    public void testSubstructFail() {
        Calculator calculator = new Calculator();
        Assert.assertEquals(calculator.substruct(4.0,2.0),5.0);
    }

    @Test
    public void testDivideFail() {
        Calculator calculator = new Calculator();
        Assert.assertEquals(calculator.divide(6.0,2.0),4.0);
    }

    @Test
    public void testMultiplyFail() {
        Calculator calculator = new Calculator();
        Assert.assertEquals(calculator.multiply(3.0,2.0),8.0);
    }

}
